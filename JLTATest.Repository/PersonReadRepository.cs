﻿using JLTATest.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JLTATest.Repository
{
   public class PersonReadRepository
    {
        private readonly string connectionString = "";
        public PersonReadRepository(string conn)
        {
            connectionString = conn;
        }

        public ResponseDTO GetAll()
        {
            ResponseDTO res = new ResponseDTO();

            try
            {
       
                string query = "SELECT P.Id,P.FirstName,P.MiddleName,P.LastName,P.NRIC,S.Description AS CurrentStatus FROM Person AS P inner join Status AS S ON P.CurrentStatus = S.Id";
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter(query, con))
                    {
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            res.Data = dt;
                            res.IsSuccess = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                res.IsSuccess = false;
                res.Error = ex.ToString();
            }

            return res;
        }

        public ResponseDTO SearchByKey(string key)
        {
            ResponseDTO res = new ResponseDTO();

            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        string sql = "SELECT P.Id,P.FirstName,P.MiddleName,P.LastName,P.NRIC,S.Description AS CurrentStatus FROM Person AS P inner join Status AS S ON P.CurrentStatus = S.Id";
                        if (!string.IsNullOrEmpty(key.Trim()))
                        {
                            sql += " WHERE P.FirstName LIKE @SearchText + '%' OR P.MiddleName LIKE @SearchText + '%' OR P.LastName LIKE @SearchText + '%' OR P.NRIC LIKE @SearchText + '%'";
                            cmd.Parameters.AddWithValue("@SearchText", key.Trim());
                        }
                        cmd.CommandText = sql;
                        cmd.Connection = con;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            DataTable dt = new DataTable();
                            sda.Fill(dt);
                            res.Data = dt;
                            res.IsSuccess = true;
                        }
                    }
                }




                //string query = "SELECT P.Id,P.FirstName,P.MiddleName,P.LastName,P.NRIC,S.Description AS CurrentStatus FROM Person AS P inner join Status AS S ON P.CurrentStatus = S.Id";
                //using (SqlConnection con = new SqlConnection(connectionString))
                //{
                //    using (SqlDataAdapter sda = new SqlDataAdapter(query, con))
                //    {
                //        using (DataTable dt = new DataTable())
                //        {
                //            sda.Fill(dt);
                //            res.Data = dt;
                //            res.IsSuccess = true;
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                res.IsSuccess = false;
                res.Error = ex.ToString();
            }

            return res;
        }

        public ResponseDTO GetByNameAndNRIC(PersonDTO person)
        {
            ResponseDTO res = new ResponseDTO();

            try
            {

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        string sql = "SELECT * FROM Person WHERE (FirstName=@FName AND MiddleName=@MName AND LastName=@LName)";
                        if (!string.IsNullOrEmpty(person.NRIC))
                        {
                            sql = sql + " OR NRIC=@NRIC";
                            cmd.Parameters.AddWithValue("@NRIC", person.NRIC);
                        }
                        cmd.Parameters.AddWithValue("@FName", person.FirstName);
                        cmd.Parameters.AddWithValue("@MName", person.MiddleName);
                        cmd.Parameters.AddWithValue("@LName", person.LastName);
                        
                        cmd.CommandText = sql;
                        cmd.Connection = con;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            DataTable dt = new DataTable();
                            sda.Fill(dt);
                            res.Data = dt;
                            res.IsSuccess = true;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                res.IsSuccess = false;
                res.Error = ex.ToString();
            }

            return res;
        }
    }
}
