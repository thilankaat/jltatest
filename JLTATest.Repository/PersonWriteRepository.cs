﻿using JLTATest.DTO;
using JLTATest.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JLTATest.Repository
{
   public class PersonWriteRepository
    {
        private readonly string connectionString ="";
        public PersonWriteRepository(string conn)
        {
            connectionString = conn;
        }

        public ResponseDTO Save(PersonDTO person)
        {
            ResponseDTO res = new ResponseDTO();
            try
            {
                string query = "INSERT INTO Person VALUES(@FirstName, @MiddleName,@LastName,@NRIC,@DateCreated,@DateLastActivated,@DateLastDeactivated,@CurrentStatus)";

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(query))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", person.FirstName);
                        cmd.Parameters.AddWithValue("@MiddleName", person.MiddleName);
                        cmd.Parameters.AddWithValue("@LastName", person.LastName);
                        cmd.Parameters.AddWithValue("@NRIC", person.NRIC);
                        cmd.Parameters.AddWithValue("@DateCreated", DateTime.Now);
                        cmd.Parameters.AddWithValue("@DateLastActivated", Convert.ToDateTime("01/01/1970"));
                        cmd.Parameters.AddWithValue("@DateLastDeactivated", Convert.ToDateTime("01/01/1970"));
                        cmd.Parameters.AddWithValue("@CurrentStatus", Status.New);
                        cmd.Connection = con;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
                res.IsSuccess = true;
                res.Message = "Person added successfully!";
                
            }
            catch (Exception ex)
            {
                res.IsSuccess = false;
                res.Error = ex.Message;
              
            }
            return res;

        }

        public ResponseDTO Update(PersonDTO person)
        {
            ResponseDTO res = new ResponseDTO();
            try
            {
                string query = "UPDATE Person SET FirstName=@FName,MiddleName=@MName,LastName=@LName WHERE Id=@Id";
               
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(query))
                    {
                        cmd.Parameters.AddWithValue("@Id", person.Id);
                        cmd.Parameters.AddWithValue("@FName", person.FirstName);
                        cmd.Parameters.AddWithValue("@MName", person.MiddleName);
                        cmd.Parameters.AddWithValue("@LName", person.LastName);
                       
                        cmd.Connection = con;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
                res.IsSuccess = true;
                res.Message = "Person updated successfully!";

            }
            catch (Exception ex)
            {
                res.IsSuccess = false;
                res.Error = ex.Message;

            }
            return res;

        }

        public ResponseDTO UpdateStatus(PersonDTO person)
        {
            ResponseDTO res = new ResponseDTO();
            try
            {
                int id = person.Id;
                string query = "UPDATE Person SET CurrentStatus=@Status,DateLastActivated=@DateLastActivated WHERE Id=@Id";
                
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(query))
                    {
                        cmd.Parameters.AddWithValue("@Id", id);
                        cmd.Parameters.AddWithValue("@Status", person.Status);
                        if (person.Status ==(int)Status.Active)
                        {
                            cmd.Parameters.AddWithValue("@DateLastActivated", DateTime.Now);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@DateLastDeactivated", DateTime.Now);
                        }
                        
                        cmd.Connection = con;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
                res.IsSuccess = true;
                if (person.Status == (int)Status.Active)
                {
                    res.Message = "Person " + person.FirstName + " activated successfully!";
                }
                else
                {
                    res.Message = "Person " + person.FirstName + " de-activated successfully!";
                }
                

            }
            catch (Exception ex)
            {
                res.IsSuccess = false;
                res.Error = ex.Message;
                res.Message = "Error occured while updating status of "+person.FirstName;

            }
            return res;

        }
    }
}
