﻿using JLTATest.DTO;
using JLTATest.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JLTATest.Service
{
   public class PersonReadService
    {
        private readonly string connectionString = "";
        PersonReadRepository prRepo = null;
        public PersonReadService(string conn)
        {
            connectionString = conn;
            prRepo = new PersonReadRepository(connectionString);
        }
        public ResponseDTO GetAll()
        {
            return prRepo.GetAll();
        }

        public ResponseDTO SearchByKey(string key)
        {
            return prRepo.SearchByKey(key);
        }
        public ResponseDTO GetByNameAndNRIC(PersonDTO person)
        {
            return prRepo.GetByNameAndNRIC(person);
        }
    }
}
