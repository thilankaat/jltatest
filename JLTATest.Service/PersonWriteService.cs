﻿using JLTATest.DTO;
using JLTATest.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JLTATest.Service
{
   public class PersonWriteService
    {
        private readonly string connectionString = "";
        PersonReadService prRepo = null;
        PersonWriteRepository pwRepo = null;
        public PersonWriteService(string conn)
        {
            connectionString = conn;
            prRepo = new PersonReadService(connectionString);
            pwRepo = new PersonWriteRepository(connectionString);
        }

        public ResponseDTO UpdateStatus(PersonDTO person)
        {
            return pwRepo.UpdateStatus(person);
        }
        public ResponseDTO Save(PersonDTO person)
        {
            ResponseDTO response = new ResponseDTO();
            ResponseDTO resRead = prRepo.GetByNameAndNRIC(person);
            if (resRead.IsSuccess)
            {
                if (resRead.Data.Rows.Count > 0)
                {
                    response.IsSuccess = false;
                    response.Error = "Person already exists!";
                }
                else
                {
                    if (person.Id > 0)
                    {
                        return pwRepo.Update(person);
                    }
                    else
                    {
                        return pwRepo.Save(person);
                    }
                    
                }
            }
            else
            {
                response.IsSuccess = false;
                response.Error = resRead.Error;
            }
            return response;
        }
    }
}
