﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JLTATest.DTO
{
   public class ResponseDTO
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public DataTable Data { get; set; }
        public string Error { get; set; }
    }
}
