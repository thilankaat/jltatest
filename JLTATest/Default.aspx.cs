﻿using JLTATest.DTO;
using JLTATest.Service;
using JLTATest.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JLTATest
{
    public partial class _Default : Page
    {
        string constr = "";
        PersonWriteService pWriteService = null;
        PersonReadService pReadService = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            constr= ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            pWriteService = new PersonWriteService(constr);
            pReadService = new PersonReadService(constr);
            if (!this.IsPostBack)
            {
                this.BindGrid();
            }
        }

        private void BindGrid()
        {
            ResponseDTO res = new ResponseDTO();
            string searchText = txtSearch.Text;
            if (string.IsNullOrEmpty(searchText) || searchText=="")
            {
                res = pReadService.GetAll();
            }
            else
            {
                res = pReadService.SearchByKey(searchText);
            }
            // res = pReadService.GetAll();
            if (res.IsSuccess)
            {
                GridView1.DataSource = res.Data;
                GridView1.DataBind();
            }
            else
            {
                SetAlertMassage(res.Error, "ERROR!");
            }
           
        }

        protected void Insert(object sender, EventArgs e)
        {
            PersonDTO person = new PersonDTO();
            person.FirstName = txtFName.Text;
            person.MiddleName = txtMName.Text;
            person.LastName = txtLName.Text;
            person.NRIC = txtNRIC.Text;
          
            ResponseDTO res = pWriteService.Save(person);
            if (res.IsSuccess)
            {
                this.BindGrid();
                ClearAddForm();
                SetAlertMassage(res.Message, "SUCCESS!");
            }
            else
            {
                SetAlertMassage(res.Error, "ERROR!");
            }
            
        }
        private void ClearAddForm()
        {
            txtFName.Text="";
            txtMName.Text="";
            txtLName.Text="";
            txtNRIC.Text="";
        }
        private void SetAlertMassage(string massage,string header)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), header, "alert('"+ massage + "')", true);
        }
        protected void OnRowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            this.BindGrid();
        }

        protected void OnRowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = GridView1.Rows[e.RowIndex];
            PersonDTO person = new PersonDTO();
            person.Id= Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0]);
            person.FirstName = (row.FindControl("txtFName") as TextBox).Text;
            person.MiddleName = (row.FindControl("txtMName") as TextBox).Text;
            person.LastName = (row.FindControl("txtLName") as TextBox).Text;
           // person.NRIC = (row.FindControl("txtNRIC") as TextBox).Text;
            
            ResponseDTO res = pWriteService.Save(person);
            if (res.IsSuccess)
            {
                GridView1.EditIndex = -1;
                this.BindGrid();
                SetAlertMassage(res.Message, "SUCCESS!");
            }
            else
            {
                SetAlertMassage(res.Error, "ERROR!");
                //}

            }
            
        }

        protected void OnRowCancelingEdit(object sender, EventArgs e)
        {
            GridView1.EditIndex = -1;
            this.BindGrid();
        }

       

        protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != GridView1.EditIndex)
            //{
            //    (e.Row.Cells[6].Controls[2] as LinkButton).Attributes["onclick"] = "return confirm('Do you want to delete this row?');";
            //}
        }
        protected void Search(object sender, EventArgs e)
        {
            this.SearchCustomers();
        }
        private void SearchCustomers()
        {
            ResponseDTO res = pReadService.SearchByKey(txtSearch.Text.Trim());
            if (res.IsSuccess)
            {
                GridView1.DataSource = res.Data;
                GridView1.DataBind();
            }
            else
            {
                SetAlertMassage(res.Error, "ERROR!");
            }
           
        }
        protected void OnPaging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            this.BindGrid();
        }

        protected void btnActivate_Click(object sender, EventArgs e)
        {
            ChangeStatus(sender, e, true);
          
        }

        private void ChangeStatus(object sender, EventArgs e,bool isWantActive)
        {
            string message = "";
            foreach (GridViewRow gvrow in GridView1.Rows)
            {
                var checkbox = gvrow.FindControl("chkSelect") as CheckBox;
                if (checkbox.Checked)
                {
                    PersonDTO person = new PersonDTO();
                    
                    var firstName = gvrow.FindControl("lblFName") as Label;
                    person.Id = Convert.ToInt32(GridView1.DataKeys[gvrow.RowIndex].Values[0]);
                    person.FirstName=firstName.Text;
                    if (isWantActive)
                    {
                        person.Status = (int)Status.Active;
                    }
                    else
                    {
                        person.Status = (int)Status.Inactive;
                    }
                    
                    ResponseDTO response = pWriteService.UpdateStatus(person);
                    if (response.IsSuccess)
                    {
                        message = message + response.Message+" and " ;
                    }
                    else
                    {
                        message = message + response.Message + " and ";
                        message = message + " ERROR Message:" + response.Error + " and ";
                    }
                   
                }

            }
          
            SetAlertMassage(message, "Update Statuses");
            this.BindGrid();
        }
        protected void btnDeactivate_Click(object sender, EventArgs e)
        {
            ChangeStatus(sender,e,false);
        }

      
    }
}