﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="JLTATest._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div id="dvGrid" style="padding: 10px; width: 900px">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Label>Seach:</asp:Label>
                <asp:TextBox ID="txtSearch" runat="server" OnTextChanged="Search" AutoPostBack="true"></asp:TextBox>

                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" OnRowDataBound="OnRowDataBound"
                    DataKeyNames="Id" OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
                    PageSize="15" AllowPaging="true" OnPageIndexChanging="OnPaging" OnRowUpdating="OnRowUpdating"
                     EmptyDataText="No records has been added." Width="900">
                    <Columns>
                        <asp:TemplateField HeaderText="Select Data">  
                    <EditItemTemplate>  
                        <asp:CheckBox ID="chkSelect" runat="server" />  
                    </EditItemTemplate>  
                    <ItemTemplate>  
                        <asp:CheckBox ID="chkSelect" runat="server" />  
                    </ItemTemplate>  
                </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name" ItemStyle-Width="200">
                            <ItemTemplate>
                                <asp:Label ID="lblFName" runat="server" Text='<%# Eval("FirstName") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFName" runat="server" Text='<%# Eval("FirstName") %>' Width="140"></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="error" runat="server" id="reqFName" controltovalidate="txtFName" errormessage="Please enter first name!" />
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Middle Name" ItemStyle-Width="200">
                            <ItemTemplate>
                                <asp:Label ID="lblMName" runat="server" Text='<%# Eval("MiddleName") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMName" runat="server" Text='<%# Eval("MiddleName") %>' Width="140"></asp:TextBox>
                                  <asp:RequiredFieldValidator CssClass="error" runat="server" id="reqMName" controltovalidate="txtMName" errormessage="Please enter niddle name!" />
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Last Name" ItemStyle-Width="200">
                            <ItemTemplate>
                                <asp:Label ID="lblLName" runat="server" Text='<%# Eval("LastName") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtLName" runat="server" Text='<%# Eval("LastName") %>' Width="140"></asp:TextBox>
                                 <asp:RequiredFieldValidator CssClass="error" runat="server" id="reqLName" controltovalidate="txtLName" errormessage="Please enter last name!" />
                            </EditItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="NRIC" ItemStyle-Width="200">
                            <ItemTemplate>
                                <asp:Label ID="lblNRIC" runat="server" Text='<%# Eval("NRIC") %>'></asp:Label>
                            </ItemTemplate>
                           <%-- <EditItemTemplate>
                                <asp:TextBox ID="txtNRIC" runat="server" Text='<%# Eval("NRIC") %>' Width="140"></asp:TextBox>
                                  <asp:RequiredFieldValidator runat="server" id="reqNRIC" controltovalidate="txtNRIC" errormessage="Please enter NRIC!" />
                            </EditItemTemplate>--%>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="Status" ItemStyle-Width="200">
                            <ItemTemplate>
                                <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("CurrentStatus") %>'></asp:Label>
                            </ItemTemplate>
                           
                        </asp:TemplateField>
                        <asp:CommandField ButtonType="Link" ShowEditButton="true" 
                            ItemStyle-Width="150" />
                    </Columns>
                </asp:GridView>
                <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse">
                    <tr>
                        <td style="width: 200px">
                           First Name:<br />
                            <asp:TextBox ID="txtFName" runat="server" Width="140" />
                              <%--<asp:RequiredFieldValidator runat="server" id="reqAddFName" controltovalidate="txtFName" errormessage="Please enter first name!" />--%>
                        </td>
                         <td style="width: 200px">
                           Middle Name:<br />
                            <asp:TextBox ID="txtMName" runat="server" Width="140" />
                              <%-- <asp:RequiredFieldValidator runat="server" id="reqAddMName" controltovalidate="txtMName" errormessage="Please enter middle name!" />--%>
                        </td>
                         <td style="width: 200px">
                           Last Name:<br />
                            <asp:TextBox ID="txtLName" runat="server" Width="140" />
                              <%-- <asp:RequiredFieldValidator runat="server" id="redAddLName" controltovalidate="txtLName" errormessage="Please enter last name!" />--%>
                        </td>
                         <td style="width: 200px">
                            NRIC:<br />
                            <asp:TextBox ID="txtNRIC" runat="server" Width="140" />
                              <%-- <asp:RequiredFieldValidator runat="server" id="reqAddNRIC" controltovalidate="txtNRIC" errormessage="Please enter NRIC!" />--%>
                        </td>
                        
                        <td style="width: 150px">
                            <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="Insert" onClientClick="return validateData();" />
                        </td>
                    </tr>
                </table>
                <asp:Button ID="btnActivate" runat="server" Text="Activate" OnClick="btnActivate_Click" />
                 <asp:Button ID="btnDeactivate" runat="server" Text="De Activate" OnClick="btnDeactivate_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
   
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="scripts/jquery.blockUI.js"></script>
        <script type="text/javascript">
            function validateData() {
                var firstName = document.getElementById('<%=txtFName.ClientID%>').value;
                var middleName = document.getElementById('<%=txtMName.ClientID%>').value;
                var lastName = document.getElementById('<%=txtLName.ClientID%>').value;
                var nric = document.getElementById('<%=txtNRIC.ClientID%>').value;
                if (firstName == "" || middleName == "" || lastName == "" || nric == "") {
                    alert("Please fill all person details");
                    return false;
                }
                else {
                    return true;
                }
            }
        $(function () {
            BlockUI("dvGrid");
            $.blockUI.defaults.css = {};

        });
        function BlockUI(elementID) {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(function () {
                $("#" + elementID).block({ message: '<div align = "center">' + '<img src="images/loadingAnim.gif"/></div>',
                    css: {},
                    overlayCSS: { backgroundColor: '#000000', opacity: 0.6, border: '3px solid #63B2EB' }
                });
            });
            prm.add_endRequest(function () {
                $("#" + elementID).unblock();
            });
        };
    </script>

     <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 10pt;
        }
        table
        {
            border: 1px solid #ccc;
            border-collapse: collapse;
            background-color: #fff;
        }
        table th
        {
            background-color: #B8DBFD;
            color: #333;
            font-weight: bold;
        }
        table th, table td
        {
            padding: 5px;
            border: 1px solid #ccc;
        }
        table, table table td
        {
            border: 0px solid #ccc;
        }
        .error{
            color:red;
        }
    </style>

</asp:Content>
