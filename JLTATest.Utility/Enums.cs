﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JLTATest.Utility
{
    public enum Status
    {
        New = 1,
        Active = 2,
        Inactive = 3,
    }
}
